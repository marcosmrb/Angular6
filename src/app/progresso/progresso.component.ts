import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progresso',
  templateUrl: './progresso.component.html',
  styleUrls: ['./progresso.component.scss']
})
export class ProgressoComponent implements OnInit {
  @Input()
  public totalSteps = 8;
  @Input()
  public stepAtual = 0;

  public progresso = 0;

  constructor() { 
    
  }

  ngOnInit() {
    console.log(this.totalSteps);
    console.log(this.stepAtual);
  }

  get Progresso(){
    return (100 * this.stepAtual) / this.totalSteps;
  }

}
