import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Coracao } from '../shared/coracao.model';

@Component({
  selector: 'app-tentativas',
  templateUrl: './tentativas.component.html',
  styleUrls: ['./tentativas.component.scss']
})
export class TentativasComponent implements OnInit, OnChanges {
  @Input()
  public tentativas: number;
  
  public coracoes: Array<Coracao> = [
    new Coracao(true),
    new Coracao(true),
    new Coracao(true)
  ];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(dados) {
    if(dados.tentativas.previousValue  && dados.tentativas.previousValue !== this.tentativas){
      this.tentativas = dados.tentativas.currentValue;
      const index = this.coracoes.length - this.tentativas;
      this.coracoes[index - 1].flagCheio= false;
      
    }
    
  }

}
