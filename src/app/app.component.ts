import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public jogoEmAndameto = true;
  public tipoEncerramento: string;

  public encerrarJogo(tipo:string){
    this.jogoEmAndameto = false;
    this.tipoEncerramento = tipo;
  }

  public resetarJogo(){
    this.jogoEmAndameto = true;
  }
}
