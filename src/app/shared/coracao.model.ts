export class Coracao{
    public urlCheio: string;
    public urlVazio: string;
    public flagCheio: boolean;

    constructor(
        flagCheio: boolean,
        urlVazio: string = 'assets/coracao_vazio.png',
        urlCheio: string = 'assets/coracao_cheio.png'
    ){
        this.flagCheio = flagCheio;
        this.urlCheio = urlCheio;
        this.urlVazio = urlVazio;
    }

    get Url(): string {
        if(this.flagCheio)
            return this.urlCheio

        return this.urlVazio;
    }
}