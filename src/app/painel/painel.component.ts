import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Frase } from '../shared/frase.model';
import { FRASES } from './frases.mock';
import { HtmlParser } from '@angular/compiler';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.scss']
})
export class PainelComponent implements OnInit {

  @Output()
  public encerrarJogo = new EventEmitter();

  public frases: Array<Frase> = FRASES;
  public instruction = 'Traduza a frase';
  public resposta: string;
  public rodada: number;
  public quantidadeRodadas: number;
  public rodadaFrase: Frase;
  public tentativas: number;

  constructor() {
    this.resposta = 'teste1';
    this.rodada = 0;
    this.tentativas = 3;
    this.quantidadeRodadas = this.frases.length;
    this.setRodadaAtual();
  }

  ngOnInit() {
    console.log(this.frases);
  }

  public setRodadaAtual() {
    this.rodadaFrase = this.frases[this.rodada];
    this.resposta = '';
  }

  public setResposta(value) {
    this.resposta = value;
  }

  private comparaResposta() {
    return this.rodadaFrase.frasePtBr.toLocaleLowerCase() === this.resposta.trim().toLocaleLowerCase();
  }

  public verificaVitoria(){
    if(this.rodada === this.quantidadeRodadas){
      this.encerrarJogo.emit('Vitória');
    }
  }

  public verificaDerrota(){
    if(this.tentativas===-1){
      this.encerrarJogo.emit('Você perdeu');
    }
  }

  public verificarResposta(value) {
    console.log('verificarResposta', this.rodada);

    if (this.comparaResposta()) {
      this.rodada++;
      this.setRodadaAtual();
      this.verificaVitoria();
    }
    else {
      this.tentativas--;
      this.verificaDerrota();
    }
  }
}
